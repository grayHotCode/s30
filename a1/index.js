const express = require('express');
const mongoose = require('mongoose');

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));


mongoose.connect('mongodb+srv://admin:admin131@zuittbootcamp.wyjct.mongodb.net/session30?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

	
	db.on("error", console.error.bind(console, "Connection Error"))

	
	db.once('open', () => console.log('Connected to the cloud database'))


const userSchema = new mongoose.Schema({

	username: {
		type: String,
		minlength: 8
	},
	password: {
		type: String,
		minlength: 6
	}
	
});

const User = mongoose.model('User', userSchema);


app.post('/signup', (req, res) => {

	// check any duplicate task
	// err shorthand of error
	User.findOne({username: req.body.username}, (err, result) =>{

		// if there are matches,
		if(result != null && result.username === req.body.username){

			// will return this message
			return res.send('Duplicate user found.')

		} else {

			let newUser = new User({

				username: req.body.username,
				password: req.body.password
			})

			// save method will accept a callback function which stores any errors in the first parameter and will store the newly saved document in the second parameter
			newUser.save((saveErr, savedUser) => {

				// if there are any errors, it will print the error.
				if(saveErr){
					return console.error(saveErr)

				//no error found, return the status code and the message.
				} else{

					return res.status(201).send('New User Created')
				}
			})
		}
	})
});


app.get('/signup', (req, res) => {

	// 'find is a mongoose method that is similar with mongoDB 'find' and an empty '{}' means it returns all the documents and stores them in the 'result' parameter of the callback function.
	User.find({}, (err, result) => {

		// if an error occured, it will print out the found error.
		if(err){

			return console.log(err);

		// if no errors were found, it will show the results.
		}else {

			// json method allows to send JSON format for the response.
			return res.status(200).json({

				data: result
			})
		}
	})
})


app.listen(port, () => console.log(`Server is running at port ${port}`))